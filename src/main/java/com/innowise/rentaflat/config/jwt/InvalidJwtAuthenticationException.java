package com.innowise.rentaflat.config.jwt;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;

public class InvalidJwtAuthenticationException extends AuthenticationException {

    private HttpStatus statuc;

    public InvalidJwtAuthenticationException(String e) {
        super(e);
    }

    public InvalidJwtAuthenticationException(HttpStatus status, String e) {
        super(e);
        this.statuc = status;
    }
}