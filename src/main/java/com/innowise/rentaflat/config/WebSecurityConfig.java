package com.innowise.rentaflat.config;


import com.innowise.rentaflat.config.jwt.JwtConfigurer;
import com.innowise.rentaflat.config.jwt.JwtTokenProvider;
import com.innowise.rentaflat.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.DELETE, "/adverts/*", "/users/*", "/cars", "/cars/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT, "/adverts/*", "/users/*", "/cars", "/cars/**").hasAuthority("USER")
                .antMatchers(HttpMethod.POST, "/adverts", "/users", "/cars", "/cars/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/adverts/*").permitAll()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/adverts", "/adverts/*", "/adverts/**", "/cars", "/cars/**").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
    }


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}

