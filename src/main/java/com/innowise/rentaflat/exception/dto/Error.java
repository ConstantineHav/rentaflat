package com.innowise.rentaflat.exception.dto;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@Builder
public class Error {

    private final String message;
    @Builder.Default
    private LocalDateTime time = LocalDateTime.now();

    private HttpStatus status;
}
