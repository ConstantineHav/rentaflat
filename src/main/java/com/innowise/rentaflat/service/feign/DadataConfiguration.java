package com.innowise.rentaflat.service.feign;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;

public class DadataConfiguration {

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header("Authorization", "Token 24e2f0915508ae3ff5415a1bfd2a0729f1405329");
            requestTemplate.header("X-Secret", "dcebab7ef183217058a6ff159199a98ae47c80c1");
        };
    }
}
