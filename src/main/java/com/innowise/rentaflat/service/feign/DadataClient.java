package com.innowise.rentaflat.service.feign;


import com.innowise.rentaflat.repository.entity.AddressInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@FeignClient(url = "${dadata.api.uri}", name = "${dadata.api.name}", configuration = DadataConfiguration.class)
public interface DadataClient {

    @PostMapping("/address")
    List<AddressInfo> getAddressInfo(@RequestBody List<String> addresses);

}
