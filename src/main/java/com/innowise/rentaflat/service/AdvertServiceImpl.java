package com.innowise.rentaflat.service;

import com.innowise.rentaflat.controller.dto.AdvertDto;
import com.innowise.rentaflat.exception.BusinessException;
import com.innowise.rentaflat.repository.AdvertRepo;
import com.innowise.rentaflat.repository.entity.AddressInfo;
import com.innowise.rentaflat.repository.entity.Advert;
import com.innowise.rentaflat.service.converter.AdvertConverter;
import com.innowise.rentaflat.service.feign.DadataClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.redcom.lib.integration.api.client.dadata.dto.Address;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.innowise.rentaflat.service.CoordinateFinder.getGeoCoordinates;

@RequiredArgsConstructor
@Service
@Slf4j
public class AdvertServiceImpl implements AdvertService {

    private final AdvertRepo advertRepo;
    private final DadataClient dadataClient;

    @Override
    public List<AdvertDto> findAll() {

        return advertRepo
                .findAll()
                .stream()
                .map(AdvertConverter::convertAdvertEntityToAdvertDto)
                // disabled - need more money for this operation
                //.map(this::setGeoCoordinatesToAdvertDto)
                .collect(Collectors.toList());
    }


    private AdvertDto setGeoCoordinatesToAdvertDto(AdvertDto advert) {
        Address address = getGeoCoordinates(advert);

        advert.setLatitude(address.getGeoLat());
        advert.setLongitude(address.getGeoLon());

        return advert;
    }


    @Override
    public AdvertDto findById(Long advertId) {


        Optional<AdvertDto> advert = advertRepo.findById(advertId)
                .map(AdvertConverter::convertAdvertEntityToAdvertDto);

        List<String> strings = new ArrayList<>();
        strings.add(advert.get().getAddress());

        List<AddressInfo> address = dadataClient.getAddressInfo(strings);

        advert.get().setLatitude(Double.valueOf(address.get(0).getGeoLat()));
        advert.get().setLongitude(Double.valueOf(address.get(0).getGeoLon()));

        return advert.orElseThrow(
                () -> new BusinessException("Advert not found!", HttpStatus.NOT_FOUND));
    }

    @Override
    public void saveAdvert(Advert advertEntity) {
        advertRepo.save(advertEntity);
    }

    @Override
    public void editAdvert(Advert advert) {

        Optional<Advert> oldAdvert = Optional.ofNullable(advertRepo.findById(advert.getId())
                .orElse(advertRepo.findByAddressAndFloorAndTotalRooms(
                        advert.getAddress(),
                        advert.getFloor(),
                        advert.getTotalRooms())));

        if (oldAdvert.isEmpty()) {
            throw new BusinessException("Advert not found!");
        }

        advertRepo.save(advert);
    }

    @Override
    public void deleteAdvert(Long advertId) {
        advertRepo.deleteById(advertId);
    }

}
