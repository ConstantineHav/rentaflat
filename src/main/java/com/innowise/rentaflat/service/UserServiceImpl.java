package com.innowise.rentaflat.service;

import com.innowise.rentaflat.controller.dto.UserDto;
import com.innowise.rentaflat.exception.BusinessException;
import com.innowise.rentaflat.repository.UserRepo;
import com.innowise.rentaflat.repository.entity.User;
import com.innowise.rentaflat.service.converter.UserConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.innowise.rentaflat.service.converter.UserConverter.convertUserEntityToUserDto;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return Optional.ofNullable(userRepo.findByUsername(username))
                .orElseThrow(() -> new UsernameNotFoundException("User not found!"));
    }

    @Override
    public UserDto findById(Long userId) {

        return convertUserEntityToUserDto(userRepo.findById(userId)
                .orElseThrow(() -> new BusinessException("User not found!", HttpStatus.NOT_FOUND)));
    }


    @Override
    public boolean addUser(User user) {

        Optional<User> checkingUser = Optional
                .ofNullable(userRepo.findByUsername(user.getUsername()));

        if (checkingUser.isPresent()) {
            return false;
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        userRepo.save(user);
        return true;
    }


    @Override
    public List<UserDto> findAll() {
        return userRepo
                .findAll()
                .stream()
                .map(UserConverter::convertUserEntityToUserDto)
                .collect(Collectors.toList());
    }


    @Override
    public void updateUser(User user) {
        User userFromDb = findByUserId(user.getUserId());
        if (!userFromDb.equals(user)) {
            userRepo.save(user);
        }
    }

    private User findByUserId(Long userId) {
        return userRepo.findById(userId)
                .orElseThrow(() -> new BusinessException("User not found!"));
    }

    @Override
    public void deleteUser(Long userId) {
        userRepo.deleteById(userId);
    }
}
