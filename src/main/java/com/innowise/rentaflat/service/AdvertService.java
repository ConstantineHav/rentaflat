package com.innowise.rentaflat.service;

import com.innowise.rentaflat.controller.dto.AdvertDto;
import com.innowise.rentaflat.repository.entity.Advert;

import java.util.List;

public interface AdvertService {


    List<AdvertDto> findAll();

    AdvertDto findById(Long advertId);

    void saveAdvert(Advert advert);

    void editAdvert(Advert advert);

    void deleteAdvert(Long advertId);
}
