package com.innowise.rentaflat.service;

import com.innowise.rentaflat.controller.dto.UserDto;
import com.innowise.rentaflat.repository.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {


    UserDto findById(Long userId);

    boolean addUser(User user);

    List<UserDto> findAll();

    void updateUser(User user);

    void deleteUser(Long userId);
}
