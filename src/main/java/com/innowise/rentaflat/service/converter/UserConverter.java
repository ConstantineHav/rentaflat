package com.innowise.rentaflat.service.converter;

import com.innowise.rentaflat.controller.dto.UserDto;
import com.innowise.rentaflat.repository.entity.User;
import lombok.experimental.UtilityClass;

@UtilityClass
public class UserConverter {

    public static User convertUserDtoToUserEntity(UserDto userDto) {

        return User.builder()
                .username(userDto.getUsername())
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .middleName(userDto.getMiddleName())
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .phone(userDto.getPhone())
                .active(userDto.isActive())
                .registerDate(userDto.getRegisterDate())
                .comments(userDto.getComments())
                .build();
    }

    public static UserDto convertUserEntityToUserDto(User user) {

        return UserDto.builder()
                .username(user.getUsername())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .middleName(user.getMiddleName())
                .email(user.getEmail())
                .password(user.getPassword())
                .phone(user.getPhone())
                .active(user.isActive())
                .registerDate(user.getRegisterDate())
                .comments(user.getComments())
                .build();
    }
}
