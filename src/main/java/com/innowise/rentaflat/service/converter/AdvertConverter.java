package com.innowise.rentaflat.service.converter;

import com.innowise.rentaflat.controller.dto.AdvertDto;
import com.innowise.rentaflat.repository.entity.Advert;
import lombok.experimental.UtilityClass;

@UtilityClass
public class AdvertConverter {

    public static Advert convertAdvertDtoToAdvertEntity(AdvertDto advertDto) {
        return Advert.builder()
                .address(advertDto.getAddress())
                .wallMaterial(advertDto.getWallMaterial())
                .totalRooms(advertDto.getTotalRooms())
                .isBalconyExist(advertDto.getIsBalconyExist())
                .parkingSpace(advertDto.getParkingSpace())
                .floor(advertDto.getFloor())
                .totalFloorCount(advertDto.getTotalFloorCount())
                .cost(advertDto.getCost())
                .hasComment(advertDto.getHasComment())
                .flatArea(advertDto.getFlatArea())
                .ceilingHeight(advertDto.getCeilingHeight())
                .buildDate(advertDto.getBuildDate())
                .description(advertDto.getDescription())
                .active(advertDto.isActive())
                .comments(advertDto.getComments())
                .images(advertDto.getImages())
                .owner(advertDto.getOwner())
                .build();
    }

    public static AdvertDto convertAdvertEntityToAdvertDto(Advert advert) {
        return AdvertDto.builder()
                .address(advert.getAddress())
                .wallMaterial(advert.getWallMaterial())
                .totalRooms(advert.getTotalRooms())
                .isBalconyExist(advert.getIsBalconyExist())
                .parkingSpace(advert.getParkingSpace())
                .floor(advert.getFloor())
                .totalFloorCount(advert.getTotalFloorCount())
                .cost(advert.getCost())
                .hasComment(advert.getHasComment())
                .flatArea(advert.getFlatArea())
                .ceilingHeight(advert.getCeilingHeight())
                .buildDate(advert.getBuildDate())
                .description(advert.getDescription())
                .active(advert.isActive())
                .comments(advert.getComments())
                .images(advert.getImages())
                .owner(advert.getOwner())
                .build();
    }
}
