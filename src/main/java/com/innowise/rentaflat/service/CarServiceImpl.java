package com.innowise.rentaflat.service;

import com.innowise.rentaflat.exception.BusinessException;
import com.innowise.rentaflat.repository.CarRepo;
import com.innowise.rentaflat.repository.entity.Car;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
@Slf4j
public class CarServiceImpl implements CarService {


    private final CarRepo carRepo;


    @Override
    public List<Car> getAllCars() {
        return carRepo.findAll();
    }

    @Override
    public Car getCarFullInfo(Long carId) {

        return carRepo.findById(carId)
                .orElseThrow(() -> new BusinessException("Car not found!", HttpStatus.NOT_FOUND));
    }

    @Override
    public void createCar(Car car) {
        carRepo.save(car);
    }

    @Override
    public void editCar(Car car) {

        Optional<Car> oldCar = Optional.ofNullable(carRepo.findById(car.getId())
                .orElse(carRepo.findByBrandAndType(
                        car.getBrand(),
                        car.getType())));

        if (oldCar.isEmpty()) {
            throw new BusinessException("Car not found!");
        }

        carRepo.save(car);
    }

    @Override
    public void deleteCar(Long carId) {
        carRepo.deleteById(carId);
    }
}
