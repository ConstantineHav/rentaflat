package com.innowise.rentaflat.service;

import com.innowise.rentaflat.controller.dto.AdvertDto;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import ru.redcom.lib.integration.api.client.dadata.DaDataClient;
import ru.redcom.lib.integration.api.client.dadata.DaDataClientFactory;
import ru.redcom.lib.integration.api.client.dadata.dto.Address;

@Slf4j
@UtilityClass
public class CoordinateFinder {

    //curl -X POST \
//-H "Content-Type: application/json" \
//-H "Authorization: Token 24e2f0915508ae3ff5415a1bfd2a0729f1405329" \
//-H "X-Secret: dcebab7ef183217058a6ff159199a98ae47c80c1" \
//-d '[ "мск сухонска 11/-89" ]' \
//https://cleaner.dadata.ru/api/v1/clean/address

    private static final String API_KEY = "24e2f0915508ae3ff5415a1bfd2a0729f1405329";
    private static final String SECRET_KEY = "dcebab7ef183217058a6ff159199a98ae47c80c1";

    private final DaDataClient dadata = DaDataClientFactory.getInstance(API_KEY, SECRET_KEY);


    public static Address getGeoCoordinates(AdvertDto advertDto) {

        final Address address = dadata.cleanAddress(advertDto.getAddress());
        log.debug("cleaned address: " + address);
        return address;
    }
}
