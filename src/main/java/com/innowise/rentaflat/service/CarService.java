package com.innowise.rentaflat.service;


import com.innowise.rentaflat.repository.entity.Car;

import java.util.List;

public interface CarService {

    List<Car> getAllCars();

    Car getCarFullInfo(Long carId);

    void createCar(Car car);

    void editCar(Car car);

    void deleteCar(Long carId);

}
