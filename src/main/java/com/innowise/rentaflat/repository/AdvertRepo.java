package com.innowise.rentaflat.repository;

import com.innowise.rentaflat.repository.entity.Advert;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdvertRepo extends JpaRepository<Advert, Long> {

    Advert findByAddressAndFloorAndTotalRooms(String address,
                                              int floor,
                                              int totalRooms);
}
