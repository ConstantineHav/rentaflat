package com.innowise.rentaflat.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "adverts")
@Builder
public class Advert implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id = -1L;
    @NotBlank(message = "Address cannot be empty!")
    private String address;
    @NotNull(message = "Wall material cannot be empty!")
    @Enumerated(value = EnumType.STRING)
    private WallMaterial wallMaterial;
    @Positive
    private int totalRooms;
    @NotNull
    private Boolean isBalconyExist;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private DedicatedParkingSpace parkingSpace;
    @Positive
    private int floor;
    @Positive
    private int totalFloorCount;
    @Positive
    private BigDecimal cost;
    @NotNull
    private Boolean hasComment;

    private float flatArea;
    private float ceilingHeight;
    private LocalDate buildDate;
    private String description;
    private boolean active;
    @OneToMany(mappedBy = "commentId", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Comment> comments;
    @OneToMany(mappedBy = "imageId", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Image> images;
    @ManyToOne
    private User owner;


}
