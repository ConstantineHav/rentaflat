package com.innowise.rentaflat.repository.entity;

public enum WallMaterial {
    PANEL,
    BRICK,
    BLOCK,
    MONOLITH
}
