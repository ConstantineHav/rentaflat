package com.innowise.rentaflat.repository.entity;

public enum DedicatedParkingSpace {
    GARAGE,
    STREET,
    NO
}
