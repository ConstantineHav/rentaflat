package com.innowise.rentaflat.repository.entity;

import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@NoArgsConstructor
public enum Role implements GrantedAuthority {

    ADMIN,
    SYS_ADMIN,
    USER,
    ANON;

    @Override
    public String getAuthority() {
        return name();
    }
}
