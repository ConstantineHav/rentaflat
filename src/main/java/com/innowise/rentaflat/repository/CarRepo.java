package com.innowise.rentaflat.repository;

import com.innowise.rentaflat.repository.entity.Car;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CarRepo extends MongoRepository<Car, Long> {

    Car findByBrandAndType(String brand, String type);
}
