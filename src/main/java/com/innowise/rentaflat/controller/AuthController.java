package com.innowise.rentaflat.controller;

import com.fasterxml.uuid.Generators;
import com.innowise.rentaflat.config.jwt.InvalidJwtAuthenticationException;
import com.innowise.rentaflat.config.jwt.JwtTokenProvider;
import com.innowise.rentaflat.controller.core.AuthenticationRequest;
import com.innowise.rentaflat.controller.core.RefreshTokenRequest;
import com.innowise.rentaflat.repository.entity.Role;
import com.innowise.rentaflat.service.UserServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {

    // username: refreshToken
    private HashMap<String, String> refreshTokens;

    AuthenticationManager authenticationManager;
    JwtTokenProvider jwtTokenProvider;
    UserServiceImpl userService;

    @PostMapping("/renew")
    public String renew(@RequestBody RefreshTokenRequest data) {

        // get username
        // for now, pretend it is s
        String username = data.getUsername();
        String refreshToken = data.getRefreshToken();

        if (refreshTokens.get(refreshToken) == null) {
            throw new InvalidJwtAuthenticationException(HttpStatus.UNAUTHORIZED, "JWT error");
        }

        // check if the refreshToken is for this username
        if (refreshTokens.get(refreshToken).equals(username)) {
            // generate a new access token for this user
            //this.userService.loadUserByUsername(username));

            return jwtTokenProvider.createToken(username,
                    Collections.singletonList(Role.USER.name()));
        }

        throw new InvalidJwtAuthenticationException(HttpStatus.UNAUTHORIZED, "JWT error");


    }

    @PostMapping("/signin")
    public HashMap<String, String> signIn(@RequestBody AuthenticationRequest data) {

        try {

            String username = data.getUsername();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
            String token = jwtTokenProvider.createToken(username, Collections.singletonList(Role.USER.name()));


            log.info("Username " + username + " : password" + data.getPassword());
            log.info("Token " + token);



            // generate a refreshToken
            UUID refreshToken = Generators.randomBasedGenerator().generate();

            // store the refreshToken the username
            refreshTokens.put(refreshToken.toString(), username);
            refreshTokens.put("token", token);

            log.info("Username " + username + " : password" + data.getPassword());
            log.info("Token " + token);
            log.info("refreshToken " + refreshToken);

            return refreshTokens;
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username/password supplied");
        }
    }
}