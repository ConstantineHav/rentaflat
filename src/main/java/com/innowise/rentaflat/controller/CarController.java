package com.innowise.rentaflat.controller;

import com.innowise.rentaflat.repository.entity.Car;
import com.innowise.rentaflat.service.CarService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@RestController
public class CarController {

    static final String CARS_PATH = "/cars";
    static final String CAR_PATH = "/{carId}";

    private final CarService carService;


    @GetMapping(CARS_PATH)
    public Iterable<Car> getAllCars() {
        return carService.getAllCars();
    }

    @GetMapping(CARS_PATH + CAR_PATH)
    public Car getCarFullInfo(@PathVariable Long carId) {

        return carService.getCarFullInfo(carId);
    }

    @PostMapping(CARS_PATH)
    public void createCar(@RequestBody @Valid Car car) {

        carService.createCar(car);
    }

    @PutMapping(CARS_PATH)
    public void editCar(Car car) {

        carService.editCar(car);
    }

    @DeleteMapping(CARS_PATH + CAR_PATH)
    public void deleteCar(Long carId) {
        carService.deleteCar(carId);
    }

}
