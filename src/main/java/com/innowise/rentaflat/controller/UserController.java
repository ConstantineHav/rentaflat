package com.innowise.rentaflat.controller;

import com.innowise.rentaflat.controller.dto.UserDto;
import com.innowise.rentaflat.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.innowise.rentaflat.service.converter.UserConverter.convertUserDtoToUserEntity;


@Slf4j
//@Validated
@RequiredArgsConstructor
@RestController
public class UserController {

    static final String USERS_PATH = "/users";
    static final String USER_PATH = "/{userId}";


    private final UserService userService;

    @GetMapping(USERS_PATH)
    public List<UserDto> getAllUsers() {

        return userService.findAll();
    }

    @GetMapping(USERS_PATH + USER_PATH)
    public UserDto getUserFullInfo(@PathVariable Long userId) {

        return userService.findById(userId);
    }

    @PostMapping(USERS_PATH)
    public void createUser(@RequestBody @Valid UserDto userDto) {

        userService.addUser(convertUserDtoToUserEntity(userDto));
    }

    @PutMapping(USERS_PATH + USER_PATH)
    public void editUser(@RequestBody @Valid UserDto userDto) {

        userService.updateUser(convertUserDtoToUserEntity(userDto));
    }

    @DeleteMapping(USERS_PATH + USER_PATH)
    public void deleteUser(@PathVariable Long userId) {

        userService.deleteUser(userId);
    }
}
