package com.innowise.rentaflat.controller;

import com.innowise.rentaflat.controller.dto.AdvertDto;
import com.innowise.rentaflat.service.AdvertService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.innowise.rentaflat.service.converter.AdvertConverter.convertAdvertDtoToAdvertEntity;

@Slf4j
@Validated
@RequiredArgsConstructor
@RestController
public class AdvertController {

    static final String ADVERTS_PATH = "/adverts";
    static final String ADVERT_PATH = "/{advertId}";


    private final AdvertService advertService;


    @GetMapping(ADVERTS_PATH)
    public Iterable<AdvertDto> getAllAdverts() {
        return advertService.findAll();
    }

    @GetMapping(ADVERTS_PATH + ADVERT_PATH)
    public AdvertDto getAdvertFullInfo(@PathVariable Long advertId) {

        return (advertService.findById(advertId));
    }

    @PostMapping(ADVERTS_PATH)
    public void createAdvert(@RequestBody @Valid AdvertDto advert) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.debug("POST new advert: " + auth.getPrincipal() + " : " + auth.getCredentials() + " : " + auth.getAuthorities());

        advertService.saveAdvert(convertAdvertDtoToAdvertEntity(advert));
    }

    @PutMapping(ADVERTS_PATH + ADVERT_PATH)
    public void editAdvert(@RequestBody @Valid AdvertDto advert) {

        advertService.editAdvert(convertAdvertDtoToAdvertEntity(advert));
    }

    @DeleteMapping(ADVERTS_PATH + ADVERT_PATH)
    public void deleteAdvert(@PathVariable Long advertId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.debug("GET all adverts: " + auth.getPrincipal() + " : " + auth.getCredentials() + " : " + auth.getAuthorities());
        log.info("Trying to delete advert id= " + advertId);
        advertService.deleteAdvert(advertId);
    }
}
