package com.innowise.rentaflat.controller.dto;

import com.innowise.rentaflat.repository.entity.*;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Data
@Builder(toBuilder = true)
public class AdvertDto {

    @NotBlank
    private String address;
    @NotNull
    private WallMaterial wallMaterial;
    @Positive
    private int totalRooms;
    @NotNull
    private Boolean isBalconyExist;
    @NotNull
    private DedicatedParkingSpace parkingSpace;
    @Positive
    @Digits(integer = 2, fraction = 0)
    private int floor;
    @Positive
    private int totalFloorCount;
    @Positive
    private BigDecimal cost;
    @NotNull
    private Boolean hasComment;

    private float flatArea;
    private float ceilingHeight;
    private LocalDate buildDate;
    private String description;
    private boolean active;
    private Set<Comment> comments;
    private Set<Image> images;
    private User owner;

    private Double latitude;
    private Double longitude;

}
