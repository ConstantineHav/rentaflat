package com.innowise.rentaflat.controller.dto;

import com.innowise.rentaflat.repository.entity.Comment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
public class UserDto {


    @NotBlank(message = "USERDTO (controller) Username cannot be empty!")
    @Size(min = 6, max = 30)
    private String username;
    @Size(min = 3, max = 100)
    private String firstName;
    @Size(min = 3, max = 100)
    private String lastName;
    private String middleName;
    @Email
    private String email;
    @NotBlank(message = "USERDTO (controller) Password cannot be empty!")
    @Size(min = 3, max = 100)
    private String password;
    private String phone;
    private boolean active;
    private LocalDate registerDate;
    private Set<Comment> comments;

}