create table adverts
(
    id                bigint       not null
        constraint adverts_pkey
            primary key,
    active            boolean      not null,
    address           varchar(255),
    build_date        date,
    ceiling_height    real         not null,
    cost              numeric(19, 2),
    description       varchar(255),
    flat_area         real         not null,
    floor             integer      not null,
    has_comment       boolean      not null,
    is_balcony_exist  boolean      not null,
    parking_space     varchar(255) not null,
    total_floor_count integer      not null,
    total_rooms       integer      not null,
    wall_material     varchar(255) not null,
    owner_user_id     bigint
        constraint fki9uu4m4wxrgw3fl1tiwo00q3s
            references users
);

alter table adverts
    owner to admin;

create table comments
(
    comment_id     bigint not null
        constraint comments_pkey
            primary key
        constraint fka88m5qm9hh824hok26b4j7dkk
            references adverts,
    text           varchar(255),
    write_at       date,
    user_id        bigint
        constraint fk8omq0tc18jd43bu5tjh6jvraq
            references users,
    author_user_id bigint
        constraint fk1hjg8obsbnyiieijy039qv7mi
            references users
);

alter table comments
    owner to admin;

create table hibernate_sequences
(
    sequence_name varchar(255) not null
        constraint hibernate_sequences_pkey
            primary key,
    next_val      bigint
);
alter table hibernate_sequences
    owner to admin;

create table images
(
    image_id integer not null
        constraint images_pkey
            primary key
        constraint fkg4q7apmgdtt29h4rhm218tbxe
            references adverts,
    image    bytea
);

alter table images
    owner to admin;

create table users
(
    user_id       bigint  not null
        constraint users_pkey
            primary key,
    active        boolean not null,
    email         varchar(255),
    first_name    varchar(255),
    last_name     varchar(255),
    middle_name   varchar(255),
    password      varchar(255),
    phone         varchar(255),
    register_date date,
    username      varchar(255),
    role          varchar
);

alter table users
    owner to admin;

create table users_adverts
(
    user_user_id bigint not null,
    adverts_id   bigint not null
        constraint uk_92khxu7k3an3o96h2km05sm84
            unique,
    constraint users_adverts_pkey
        primary key (user_user_id, adverts_id)
);

alter table users_adverts
    owner to admin;


create table users_comments
(
    user_user_id        bigint not null
        constraint fkmm39cipq3xkwxegq1ppn4wg84
            references users,
    comments_comment_id bigint not null
        constraint uk_f3c2hlsa7oy34m960153fxlaw
            unique
        constraint fkq06lttgwax93n6yee511pfixe
            references comments,
    constraint users_comments_pkey
        primary key (user_user_id, comments_comment_id)
);

alter table users_comments
    owner to admin;


