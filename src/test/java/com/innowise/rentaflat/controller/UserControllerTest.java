package com.innowise.rentaflat.controller;

import com.innowise.rentaflat.controller.dto.UserDto;
import com.innowise.rentaflat.repository.UserRepo;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.nio.charset.StandardCharsets;

import static com.innowise.rentaflat.controller.UserController.USERS_PATH;
import static com.innowise.rentaflat.controller.UserController.USER_PATH;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockUser(username = "admin", authorities = {"ADMIN"}, password = "admin")
class UserControllerTest extends BaseIntegrationTest {

    private static final String USER_NUMBER_2 = "__files/testing-user-number-2-from-db.json";

    @Autowired
    private UserRepo userRepo;

    @Test
    public void getAllUsers_ShouldReturn200OkCode() throws Exception {

        mockMvc.perform(get(USERS_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print());
    }


    @Test
    public void getUserFullInfo_ShouldReturn200OkCode() throws Exception {
        String expectedUser = new String(readJsonFromFile(USER_NUMBER_2), StandardCharsets.UTF_8);

        mockMvc.perform(get(USERS_PATH + USER_PATH, 2)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(expectedUser));
    }

    @Test
    void getUserFullInfo_CheckingNonExistentUser_ShouldThrownA404Code() throws Exception {
        mockMvc.perform(get(USERS_PATH + USER_PATH, 666)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("User not found!"))
                .andDo(print());

    }


    @Test
    public void deleteUser_ShouldReturn200Okt() throws Exception {
        int idUser = 1;
        mockMvc.perform(delete(USERS_PATH + USER_PATH, idUser)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andDo(result -> mockMvc.perform(get(USERS_PATH + USER_PATH, idUser))
                        .andExpect(status().isNotFound()));
    }

    @Test
    public void createUser_ShouldReturnStatusOk() throws Exception {
        long allUsersBeforeTest = userRepo.findAll().size();

        byte[] userDtoToJson = objectMapper.writeValueAsBytes(createUserDto());

        mockMvc.perform(post(USERS_PATH)
                .content(userDtoToJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        long allUsersAfterTest = userRepo.findAll().size();

        Assert.assertTrue(allUsersAfterTest > allUsersBeforeTest);
    }


    @Test
    void createUser_NotValidInput_ShouldReturnBadRequest() throws Exception {

        UserDto userDto = createUserDto();
        userDto.setUsername("!@#$%^&*()_+");
        userDto.setFirstName("");
        userDto.setEmail("blablabla456");

        byte[] userDtoToJson = objectMapper.writeValueAsBytes(userDto);

        mockMvc.perform(post(USERS_PATH)
                .content(userDtoToJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }


}