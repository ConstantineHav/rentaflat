package com.innowise.rentaflat.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innowise.rentaflat.PostgresqlInitializer;
import com.innowise.rentaflat.controller.dto.AdvertDto;
import com.innowise.rentaflat.controller.dto.UserDto;
import com.innowise.rentaflat.repository.entity.DedicatedParkingSpace;
import com.innowise.rentaflat.repository.entity.WallMaterial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(initializers = PostgresqlInitializer.class)
public class BaseIntegrationTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;


    public byte[] readJsonFromFile(String name) throws IOException {

        return Objects.requireNonNull(this.getClass().getClassLoader().getResourceAsStream(name).readAllBytes());
    }

    AdvertDto createAdvertDto() {
        return AdvertDto.builder()
                .address("Address from test")
                .wallMaterial(WallMaterial.BLOCK)
                .totalRooms(16)
                .isBalconyExist(true)
                .parkingSpace(DedicatedParkingSpace.NO)
                .floor(2)
                .totalFloorCount(3)
                .cost(BigDecimal.valueOf(123456.7))
                .hasComment(false)
                .flatArea(850)
                .ceilingHeight(3)
                .buildDate(LocalDate.ofEpochDay(2020 - 10 - 02))
                .description("olololololo")
                .active(true)
                .comments(null)
                .images(null)
                .owner(null)
                .build();
    }

    UserDto createUserDto() {
        return UserDto.builder()
                .username("hardcore")
                .firstName("hardcore")
                .lastName("hardcore")
                .middleName("")
                .email("hardcore@tut.by")
                .password("hardcore")
                .phone(String.valueOf(1234567))
                .active(true)
                .registerDate(LocalDate.now())
                .comments(null)
                .build();
    }
}
