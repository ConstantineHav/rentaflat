package com.innowise.rentaflat.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.innowise.rentaflat.controller.dto.AdvertDto;
import com.innowise.rentaflat.repository.AdvertRepo;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.innowise.rentaflat.controller.AdvertController.ADVERTS_PATH;
import static com.innowise.rentaflat.controller.AdvertController.ADVERT_PATH;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockUser(username = "admin", authorities = {"ADMIN"}, password = "admin")
@AutoConfigureWireMock(port = 0)
class AdvertControllerTest extends BaseIntegrationTest {

    private static final String ADVERT_NUMBER_ONE = "__files/testing-advert-number-1-from-db.json.json";
    private static final String ADVERT_NUMBER_TWO = "__files/testing-advert-number-2-from-db.json.json";
    private static final String GEO_INFO_FOR_ADVERT_NUMBER_TWO = "__files/testing-returning-geo-from-address-from-dadataru-for-advert-number-2.json";

    @Autowired
    private AdvertRepo advertRepo;

    @Test
    void getAllAdverts_ShouldReturn200OkCode() throws Exception {
        mockMvc.perform(get(ADVERTS_PATH)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getAdvertFullInfo_ShouldReturn200OkCode() throws Exception {
        String expectedAdvert = new String(readJsonFromFile(ADVERT_NUMBER_TWO), StandardCharsets.UTF_8);
        String expectedGeoInfo = new String(readJsonFromFile(GEO_INFO_FOR_ADVERT_NUMBER_TWO), StandardCharsets.UTF_8);

        stubFor(WireMock.post(urlPathEqualTo("/address"))
                .withHeader(HttpHeaders.CONTENT_TYPE, equalTo( MediaType.APPLICATION_JSON_VALUE))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withStatus(HttpStatus.OK_200)
                        .withBody(expectedGeoInfo)
                ));


        mockMvc.perform(get(ADVERTS_PATH + ADVERT_PATH, 2)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(expectedAdvert));
    }

    @Test
    void getAdvertFullInfo_CheckingNonExistentAdvert_ShouldThrownA404Code() throws Exception {
        mockMvc.perform(get(ADVERTS_PATH + ADVERT_PATH, 666)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Advert not found!"))
                .andDo(print());

    }


    @Test
    void deleteAdvert_ShouldReturn200Ok() throws Exception {
        long idAdvert = 1;

        mockMvc.perform(MockMvcRequestBuilders
                .delete(ADVERTS_PATH + ADVERT_PATH, idAdvert)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());

        Assert.assertEquals(Optional.empty(), advertRepo.findById(idAdvert));
    }


    @Test
    @WithMockUser(username = "user", authorities = {"USER"}, password = "user")
    void deleteAdvert_ShouldReturn401() throws Exception {
        long idAdvert = 2;
        mockMvc.perform(MockMvcRequestBuilders
                .delete(ADVERTS_PATH + ADVERT_PATH, idAdvert)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andDo(print());
    }

    @Test
    void createAdvert_ShouldReturnStatusOk() throws Exception {

        AdvertDto advertDto = createAdvertDto();

        byte[] advertToJson = objectMapper.writeValueAsBytes(advertDto);

        mockMvc.perform(post(ADVERTS_PATH)
                .content(advertToJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        Assert.assertNotEquals(Optional.empty(), advertRepo.findByAddressAndFloorAndTotalRooms(
                advertDto.getAddress(),
                advertDto.getFloor(),
                advertDto.getTotalRooms()));

    }

    @Test
    void createAdvert_NotValidInput_ShouldReturnBadRequest() throws Exception {

        mockMvc.perform(post(ADVERTS_PATH)
                .content(getBytesFromNewAdvertDto())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }


    private byte[] getBytesFromNewAdvertDto() throws JsonProcessingException {
        AdvertDto advertDto = createAdvertDto()
                .toBuilder()
                .cost(BigDecimal.valueOf(-123.456))
                .totalRooms(0)
                .flatArea(-0)
                .build();

        return objectMapper.writeValueAsBytes(advertDto);
    }

}