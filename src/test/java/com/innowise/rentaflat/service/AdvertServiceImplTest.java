package com.innowise.rentaflat.service;

import com.innowise.rentaflat.controller.dto.AdvertDto;
import com.innowise.rentaflat.exception.BusinessException;
import com.innowise.rentaflat.repository.AdvertRepo;
import com.innowise.rentaflat.repository.entity.Advert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {AdvertServiceImpl.class})
class AdvertServiceImplTest {

    @MockBean
    private AdvertRepo mockAdvertRepo;

    @Autowired
    private AdvertServiceImpl advertService;

    Advert advert = Advert.builder()
            .address("address")
            .floor(99)
            .totalRooms(99)
            .build();


    @Test
    void findAll_ShouldReturnNotEmptyList() {

        when(mockAdvertRepo.findAll()).thenReturn(List.of(advert, advert, advert));

        List<AdvertDto> adverts = advertService.findAll();

        assertNotNull(adverts);
        assertFalse(adverts.isEmpty());
    }

    @Test
    void findById_ShouldReturnAdvert() {
        Long id = 1L;
        when(mockAdvertRepo.findById(id))
                .thenReturn(Optional.of(new Advert()));

        AdvertDto advertDto = advertService.findById(id);

        assertNotNull(advertDto);
    }

    @Test
    void findById_ShouldThrownBusinessException() {
        Long id = 100500L;
        when(mockAdvertRepo.findById(id))
                .thenThrow(new BusinessException("Advert not found!"));

        Throwable exception = assertThrows(
                BusinessException.class, () -> advertService.findById(id)
        );

        assertEquals("Advert not found!", exception.getMessage());
    }


    @Test
    void editAdvert_ShouldThrownAnBusinessException() {
        when(mockAdvertRepo.findById(advert.getId()))
                .thenThrow(new BusinessException("Advert not found!"));

        Throwable exception = assertThrows(
                BusinessException.class, () -> advertService.findById(advert.getId())
        );

        assertEquals("Advert not found!", exception.getMessage());
    }

    @Test
    void editAdvert_ShouldSuccessfullyEdit() {
        when(mockAdvertRepo.findById(advert.getId()))
                .thenReturn(Optional.ofNullable(advert));
        when(mockAdvertRepo.findByAddressAndFloorAndTotalRooms(
                advert.getAddress(),
                advert.getFloor(),
                advert.getTotalRooms()
        ))
                .thenReturn(advert);
        when(mockAdvertRepo.save(advert))
                .thenReturn(advert);
        advert.setId(5L);


        advertService.editAdvert(advert);

        verify(mockAdvertRepo, times(1)).findByAddressAndFloorAndTotalRooms(advert.getAddress(), advert.getFloor(), advert.getTotalRooms());
        verify(mockAdvertRepo, times(1)).save(advert);
    }
}