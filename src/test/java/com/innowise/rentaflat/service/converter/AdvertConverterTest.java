package com.innowise.rentaflat.service.converter;

import com.innowise.rentaflat.controller.dto.AdvertDto;
import com.innowise.rentaflat.repository.entity.Advert;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(RandomBeansExtension.class)
@SpringBootTest(classes = {AdvertConverter.class})
class AdvertConverterTest {

    @Autowired
    private AdvertConverter advertConverter;

    @Random(excludes = {"comments", "images", "owner"})
    private Advert advertEntity;

    @Random(excludes = {"comments", "images", "owner"})
    private AdvertDto advertDto;

    @RepeatedTest(5)
    void convertAdvertDtoToAdvertEntity_AllFieldsShouldBeEquals() {
        Advert advertAfterConvert = advertConverter.convertAdvertDtoToAdvertEntity(advertDto);

        assertEquals(advertAfterConvert.getAddress(), advertDto.getAddress());
        assertEquals(advertAfterConvert.getFloor(), advertDto.getFloor());
        assertEquals(advertAfterConvert.getDescription(), advertDto.getDescription());
        assertEquals(advertAfterConvert.getBuildDate(), advertDto.getBuildDate());
        assertEquals(advertAfterConvert.getIsBalconyExist(), advertDto.getIsBalconyExist());
        assertEquals(advertAfterConvert.getCost(), advertDto.getCost());
        assertEquals(advertAfterConvert.getCeilingHeight(), advertDto.getCeilingHeight());
        assertEquals(advertAfterConvert.getFlatArea(), advertDto.getFlatArea());
    }

    @RepeatedTest(5)
    void convertAdvertEntityToAdvertDto_AllFieldsShouldBeEquals() {
        AdvertDto advertDtoAfterConvert = advertConverter.convertAdvertEntityToAdvertDto(advertEntity);

        assertEquals(advertDtoAfterConvert.getAddress(), advertEntity.getAddress());
        assertEquals(advertDtoAfterConvert.getFloor(), advertEntity.getFloor());
        assertEquals(advertDtoAfterConvert.getDescription(), advertEntity.getDescription());
        assertEquals(advertDtoAfterConvert.getBuildDate(), advertEntity.getBuildDate());
        assertEquals(advertDtoAfterConvert.getIsBalconyExist(), advertEntity.getIsBalconyExist());
        assertEquals(advertDtoAfterConvert.getCost(), advertEntity.getCost());
        assertEquals(advertDtoAfterConvert.getCeilingHeight(), advertEntity.getCeilingHeight());
        assertEquals(advertDtoAfterConvert.getFlatArea(), advertEntity.getFlatArea());
    }
}