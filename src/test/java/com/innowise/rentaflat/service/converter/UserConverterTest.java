package com.innowise.rentaflat.service.converter;

import com.innowise.rentaflat.controller.dto.UserDto;
import com.innowise.rentaflat.repository.entity.User;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(RandomBeansExtension.class)
@SpringBootTest(classes = {UserConverter.class})
class UserConverterTest {

    @Autowired
    private UserConverter userConverter;

    @Random(excludes = {"comments", "role"})
    private User userEntity;

    @Random(excludes = {"comments", "role"})
    private UserDto userDto;


    @RepeatedTest(5)
    void convertUserDtoToUserEntity_AllFieldsShouldBeEquals() {
        User userAfterConvert = userConverter.convertUserDtoToUserEntity(userDto);

        assertEquals(userAfterConvert.getFirstName(), userDto.getFirstName());
        assertEquals(userAfterConvert.getLastName(), userDto.getLastName());
        assertEquals(userAfterConvert.getEmail(), userDto.getEmail());
        assertEquals(userAfterConvert.getPassword(), userDto.getPassword());
        assertEquals(userAfterConvert.getUsername(), userDto.getUsername());
        assertEquals(userAfterConvert.getPhone(), userDto.getPhone());
        assertEquals(userAfterConvert.getRegisterDate(), userDto.getRegisterDate());
    }

    @RepeatedTest(5)
    void convertUserEntityToUserDto_AllFieldsShouldBeEquals() {
        UserDto userAfterConvert = userConverter.convertUserEntityToUserDto(userEntity);

        assertEquals(userAfterConvert.getFirstName(), userEntity.getFirstName());
        assertEquals(userAfterConvert.getLastName(), userEntity.getLastName());
        assertEquals(userAfterConvert.getEmail(), userEntity.getEmail());
        assertEquals(userAfterConvert.getPassword(), userEntity.getPassword());
        assertEquals(userAfterConvert.getUsername(), userEntity.getUsername());
        assertEquals(userAfterConvert.getPhone(), userEntity.getPhone());
        assertEquals(userAfterConvert.getRegisterDate(), userEntity.getRegisterDate());
    }
}