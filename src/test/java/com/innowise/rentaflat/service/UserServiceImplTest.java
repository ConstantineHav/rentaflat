package com.innowise.rentaflat.service;

import com.innowise.rentaflat.controller.dto.UserDto;
import com.innowise.rentaflat.repository.UserRepo;
import com.innowise.rentaflat.repository.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {UserServiceImpl.class})
class UserServiceImplTest {

    @MockBean
    private UserRepo mockUserRepo;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserServiceImpl userService;

    User user = User.builder()
            .username("test")
            .password("test")
            .build();

    @Test
    void findById_ShouldReturnUser() {
        Long id = 1L;
        when(mockUserRepo.findById(id))
                .thenReturn(Optional.of(new User()));

        UserDto user = userService.findById(id);

        assertNotNull(user);
    }

    @Test
    void findById_ShouldThrownUsernameNotFoundException() {
        Long id = 100500L;
        when(mockUserRepo.findById(id))
                .thenThrow(new UsernameNotFoundException("User not found!"));

        Throwable exception = assertThrows(
                UsernameNotFoundException.class, () -> {
                    userService.findById(id);
                }
        );

        assertEquals("User not found!", exception.getMessage());
    }

    @Test
    void addUser_ShouldReturnFalse_CozUserIsExist() {

        when(mockUserRepo.findByUsername(user.getUsername()))
                .thenReturn(user);

        boolean isExist = userService.addUser(user);

        assertFalse(isExist);
    }

    @Test
    void addUser_ShouldReturnTrue_UserSuccessfullyAdded() {

        when(mockUserRepo.findByUsername(user.getUsername()))
                .thenReturn(null);
        when(mockUserRepo.save(user))
                .thenReturn(user);

        boolean isAdded = userService.addUser(user);

        assertNotEquals(user.getPassword(), "test");
        assertTrue(isAdded);
    }

    @Test
    void findAll_ShouldReturnNotEmptyList() {

        when(mockUserRepo.findAll())
                .thenReturn(List.of(user, user, user));

        List<UserDto> users = userService.findAll();

        assertNotNull(users);
        assertFalse(users.isEmpty());
    }

}
