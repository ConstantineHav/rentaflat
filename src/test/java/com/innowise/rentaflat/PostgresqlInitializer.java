package com.innowise.rentaflat;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;

public class PostgresqlInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static int POSTGRES_PORT = 5432;
    private static String POSTGRES_DB_NAME = "integration-tests-db";
    private static String POSTGRES_USERNAME = "admin";
    private static String POSTGRES_PASSWORD = "admin";
    private static String POSTGRES_IMAGE = "postgres:alpine";


    public PostgreSQLContainer postgres = new PostgreSQLContainer<>(POSTGRES_IMAGE)

            .withDatabaseName(POSTGRES_DB_NAME)
            .withUsername(POSTGRES_USERNAME)
            .withPassword(POSTGRES_PASSWORD)
            .withInitScript("data.sql");


    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        postgres.start();

        TestPropertyValues.of(
                "spring.datasource.url=" + postgres.getJdbcUrl(),
                "spring.datasource.username=" + postgres.getUsername(),
                "spring.datasource.password=" + postgres.getPassword()
        ).applyTo(configurableApplicationContext.getEnvironment());
    }
}
